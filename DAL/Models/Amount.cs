﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Amount
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string ParentType { get; set; }
        public decimal? UnitAmount { get; set; }
        public string UnitType { get; set; }
        public DateTime? EventDate { get; set; }
        public string ExchangeType { get; set; }
        public int? Year { get; set; }
        public string Category { get; set; }
        public string Period { get; set; }
    }
}
