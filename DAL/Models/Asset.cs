﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Asset
    {
        public int Id { get; set; }
        public string AssetName { get; set; }
    }
}
