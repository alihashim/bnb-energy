﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class AuditTrail
    {
        public string User { get; set; }
        public string Field { get; set; }
        public DateTime? Time { get; set; }
        public string ContractId { get; set; }
        public string ChangedFrom { get; set; }
        public string ChangedTo { get; set; }
        public string Action { get; set; }
    }
}
