﻿using System;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DAL
{
    public partial class BB_ProdContext : DbContext
    {
        public BB_ProdContext()
        {
        }

        public BB_ProdContext(DbContextOptions<BB_ProdContext> options) : base(options)
        {
        }

       

        public virtual DbSet<Amount> Amount { get; set; }
        public virtual DbSet<Asset> Asset { get; set; }
        public virtual DbSet<AuditTrail> AuditTrail { get; set; }
        public virtual DbSet<BckpContractsTracts> BckpContractsTracts { get; set; }
        public virtual DbSet<BckpLandSurveys> BckpLandSurveys { get; set; }
        public virtual DbSet<BckpTract> BckpTract { get; set; }
        public virtual DbSet<ContactDetails> ContactDetails { get; set; }
        public virtual DbSet<Contract> Contract { get; set; }
        public virtual DbSet<ContractEntity> ContractEntity { get; set; }
        public virtual DbSet<ContractRef> ContractRef { get; set; }
        public virtual DbSet<ContractsTracts> ContractsTracts { get; set; }
        public virtual DbSet<CountyList> CountyList { get; set; }
        public virtual DbSet<FileLocale> FileLocale { get; set; }
        public virtual DbSet<HistoryStatus> HistoryStatus { get; set; }
        public virtual DbSet<LandSurveys> LandSurveys { get; set; }
        public virtual DbSet<LandSurveys12032018> LandSurveys12032018 { get; set; }
        public virtual DbSet<LkpLandSurvey> LkpLandSurvey { get; set; }
        public virtual DbSet<LkpSurveyContract> LkpSurveyContract { get; set; }
        public virtual DbSet<LkpSurveyTract> LkpSurveyTract { get; set; }
        public virtual DbSet<LkpTract> LkpTract { get; set; }
        public virtual DbSet<LkpTractContract> LkpTractContract { get; set; }
        public virtual DbSet<Obligation> Obligation { get; set; }
        public virtual DbSet<Ownership> Ownership { get; set; }
        public virtual DbSet<Rate> Rate { get; set; }
        public virtual DbSet<Resolution> Resolution { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<SurveyContract> SurveyContract { get; set; }
        public virtual DbSet<Term> Term { get; set; }
        public virtual DbSet<Tract> Tract { get; set; }
        public virtual DbSet<TypeAmountCategory> TypeAmountCategory { get; set; }
        public virtual DbSet<TypeContactDetails> TypeContactDetails { get; set; }
        public virtual DbSet<TypeContract> TypeContract { get; set; }
        public virtual DbSet<TypeExchange> TypeExchange { get; set; }
        public virtual DbSet<TypeObligation> TypeObligation { get; set; }
        public virtual DbSet<TypePeriod> TypePeriod { get; set; }

        // Unable to generate entity type for table 'dbo.ReplicaInfo'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.surveys'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SyncLog'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.TractSync'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Bckp_Amounts'. Please see the warning messages.

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=bbenergy.database.windows.net;Database=BB_Prod;User Id=arrowhead;password=AH@db18!;Trusted_Connection=False;MultipleActiveResultSets=true;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Amount>(entity =>
            {
                entity.Property(e => e.Category).HasMaxLength(100);

                entity.Property(e => e.EventDate).HasColumnType("date");

                entity.Property(e => e.ExchangeType).HasMaxLength(100);

                entity.Property(e => e.ParentType).HasMaxLength(200);

                entity.Property(e => e.Period).HasMaxLength(100);

                entity.Property(e => e.UnitAmount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.UnitType).HasMaxLength(50);
            });

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AssetName).HasMaxLength(150);
            });

            modelBuilder.Entity<AuditTrail>(entity =>
            {
                entity.HasKey(e => e.ContractId);

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Action).HasMaxLength(50);

                entity.Property(e => e.ChangedFrom).HasMaxLength(50);

                entity.Property(e => e.ChangedTo).HasMaxLength(50);

                entity.Property(e => e.Field).HasMaxLength(50);

                entity.Property(e => e.Time).HasColumnType("datetime");

                entity.Property(e => e.User).HasMaxLength(50);
            });

            modelBuilder.Entity<BckpContractsTracts>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BackupId });

                entity.ToTable("Bckp_Contracts_Tracts");

                entity.Property(e => e.BackupId)
                    .HasColumnName("BackupID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BackupDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BckpLandSurveys>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BackupId });

                entity.ToTable("Bckp_Land_Surveys");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BackupId)
                    .HasColumnName("BackupID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Abstract).HasMaxLength(220);

                entity.Property(e => e.BackupDate).HasColumnType("datetime");

                entity.Property(e => e.Block).HasMaxLength(220);

                entity.Property(e => e.County).HasMaxLength(220);

                entity.Property(e => e.Lot).HasMaxLength(220);

                entity.Property(e => e.Section).HasMaxLength(220);

                entity.Property(e => e.Survey).HasMaxLength(220);
            });

            modelBuilder.Entity<BckpTract>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BackupId });

                entity.ToTable("Bckp_Tract");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BackupId)
                    .HasColumnName("BackupID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BackupDate).HasColumnType("datetime");

                entity.Property(e => e.GlobalId).HasMaxLength(1000);

                entity.Property(e => e.GrossAcres).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.Kv)
                    .HasColumnName("KV")
                    .HasColumnType("decimal(28, 6)");

                entity.Property(e => e.Npri).HasColumnName("NPRI");

                entity.Property(e => e.TractId)
                    .HasColumnName("TractID")
                    .HasMaxLength(220);

                entity.Property(e => e.TractLength).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.TractType).HasMaxLength(220);

                entity.Property(e => e.TractWidth).HasColumnType("decimal(28, 6)");
            });

            modelBuilder.Entity<ContactDetails>(entity =>
            {
                entity.ToTable("Contact_Details");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContactDetailsType).HasMaxLength(220);

                entity.Property(e => e.ContractEntityId).HasColumnName("ContractEntityID");

                entity.HasOne(d => d.ContractEntity)
                    .WithMany(p => p.ContactDetails)
                    .HasForeignKey(d => d.ContractEntityId)
                    .HasConstraintName("FK_Contact_Details_Contact");
            });

            modelBuilder.Entity<Contract>(entity =>
            {
                entity.Property(e => e.Afe)
                    .HasColumnName("AFE")
                    .HasMaxLength(50);

                entity.Property(e => e.BasisAmount).HasColumnType("decimal(18, 6)");

                entity.Property(e => e.BasisUnits).HasMaxLength(50);

                entity.Property(e => e.Contact).HasMaxLength(255);

                entity.Property(e => e.ContactSafe)
                    .HasMaxLength(255)
                    .HasComputedColumnSql("(case when [Contact] IS NULL then '' else [Contact] end)");

                entity.Property(e => e.Contactid).HasColumnName("contactid");

                entity.Property(e => e.ContractIdText)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(case when [EntityType]='TPWR' then 'W'+CONVERT([varchar](10),format([TpwrId],'00000')) else CONVERT([varchar](10),[ContractId]) end)");

                entity.Property(e => e.ContractType).HasMaxLength(100);

                entity.Property(e => e.EffectiveDate).HasColumnType("date");

                entity.Property(e => e.EntityType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FileLocation).HasMaxLength(50);

                entity.Property(e => e.Grantee).HasMaxLength(255);

                entity.Property(e => e.GranteeSafe)
                    .HasMaxLength(255)
                    .HasComputedColumnSql("(case when [Grantee] IS NULL then '' else [Grantee] end)");

                entity.Property(e => e.Granteeid).HasColumnName("granteeid");

                entity.Property(e => e.Grantor)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Grantorid).HasColumnName("grantorid");

                entity.Property(e => e.Notes).IsUnicode(false);

                entity.Property(e => e.PreviousId).HasMaxLength(20);

                entity.Property(e => e.SiteAlias).HasMaxLength(200);

                entity.Property(e => e.Status).HasMaxLength(100);

                entity.Property(e => e.Tpwr).HasColumnName("TPWR");
            });

            modelBuilder.Entity<ContractEntity>(entity =>
            {
                entity.ToTable("Contract_Entity");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Attention).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.Zip).HasMaxLength(10);
            });

            modelBuilder.Entity<ContractRef>(entity =>
            {
                entity.ToTable("Contract_Ref");

                entity.Property(e => e.ReferenceContractId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceContractType).HasMaxLength(100);

                entity.Property(e => e.ReferenceGrantee).HasMaxLength(255);
            });

            modelBuilder.Entity<ContractsTracts>(entity =>
            {
                entity.ToTable("Contracts_Tracts");
            });

            modelBuilder.Entity<CountyList>(entity =>
            {
                entity.Property(e => e.CountyName).HasMaxLength(220);
            });

            modelBuilder.Entity<FileLocale>(entity =>
            {
                entity.ToTable("File_Locale");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<HistoryStatus>(entity =>
            {
                entity.ToTable("History_Status");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId).HasColumnName("ContractID");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime2(3)");

                entity.Property(e => e.ModifiedOnDate)
                    .HasColumnType("date")
                    .HasComputedColumnSql("(CONVERT([date],[ModifiedOn]))");

                entity.Property(e => e.NewStatus).HasMaxLength(100);

                entity.Property(e => e.UserEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.HistoryStatus)
                    .HasForeignKey(d => d.ContractId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_History_Status");
            });

            modelBuilder.Entity<LandSurveys>(entity =>
            {
                entity.ToTable("Land_Surveys");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Abstract).HasMaxLength(220);

                entity.Property(e => e.AutoCompleteDisplay)
                    .IsRequired()
                    .HasMaxLength(1164)
                    .HasComputedColumnSql("(concat([County],N' County | Abstract ',[Abstract],N' | Sec ',concat([Section],case when [Section]<>N'' AND [Block]<>N'' then N' | Block ' else N'' end,[Block],case when [Block]<>N'' AND [Lot]<>N'' then N' | Lot ' else N'' end,[Lot],' | Asset: ',isnull([AssetId],''))))");

                entity.Property(e => e.Block).HasMaxLength(220);

                entity.Property(e => e.County).HasMaxLength(220);

                entity.Property(e => e.CountyAbstract)
                    .IsRequired()
                    .HasMaxLength(452)
                    .HasComputedColumnSql("(concat([County],N' | ',N'Abstract '+[Abstract]))");

                entity.Property(e => e.Lot).HasMaxLength(220);

                entity.Property(e => e.Section).HasMaxLength(220);

                entity.Property(e => e.SectionBlockLot)
                    .IsRequired()
                    .HasMaxLength(666)
                    .HasComputedColumnSql("(concat([Section],case when [Section]<>N'' AND [Block]<>N'' then N' | ' else N'' end,[Block],case when [Block]<>N'' AND [Lot]<>N'' then N' | ' else N'' end,[Lot]))");

                entity.Property(e => e.SectionText)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(CONVERT([varchar](10),[Section]))");

                entity.Property(e => e.Survey).HasMaxLength(220);
            });

            modelBuilder.Entity<LandSurveys12032018>(entity =>
            {
                entity.HasKey(e => e.Objectid);

                entity.ToTable("Land_Surveys_12032018");

                entity.Property(e => e.Objectid)
                    .HasColumnName("OBJECTID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Abstract).HasMaxLength(50);

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.Block).HasMaxLength(50);

                entity.Property(e => e.County).HasMaxLength(50);

                entity.Property(e => e.Section).HasMaxLength(50);

                entity.Property(e => e.Survey).HasMaxLength(50);
            });

            modelBuilder.Entity<LkpLandSurvey>(entity =>
            {
                entity.ToTable("Lkp_LandSurvey");

                entity.Property(e => e.Abstract).HasMaxLength(220);

                entity.Property(e => e.AssetName).HasMaxLength(150);

                entity.Property(e => e.AutoCompleteDisplay).HasMaxLength(2000);

                entity.Property(e => e.Block).HasMaxLength(220);

                entity.Property(e => e.County).HasMaxLength(220);

                entity.Property(e => e.Lot).HasMaxLength(220);

                entity.Property(e => e.Section).HasMaxLength(220);

                entity.Property(e => e.Survey).HasMaxLength(220);
            });

            modelBuilder.Entity<LkpSurveyContract>(entity =>
            {
                entity.ToTable("Lkp_Survey_Contract");

                entity.Property(e => e.ContractType).HasMaxLength(100);

                entity.Property(e => e.EntityType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Grantee).HasMaxLength(255);

                entity.Property(e => e.TotalContractAcres).HasColumnType("decimal(28, 6)");
            });

            modelBuilder.Entity<LkpSurveyTract>(entity =>
            {
                entity.ToTable("Lkp_Survey_Tract");

                entity.Property(e => e.GrossAcres).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.Npri).HasColumnName("NPRI");

                entity.Property(e => e.TractId).HasMaxLength(220);
            });

            modelBuilder.Entity<LkpTract>(entity =>
            {
                entity.ToTable("Lkp_Tract");

                entity.Property(e => e.TractId).HasMaxLength(220);

                entity.Property(e => e.TractType).HasMaxLength(220);
            });

            modelBuilder.Entity<LkpTractContract>(entity =>
            {
                entity.ToTable("Lkp_Tract_Contract");

                entity.Property(e => e.Block).HasMaxLength(220);

                entity.Property(e => e.ContractIdText).HasMaxLength(50);

                entity.Property(e => e.ContractType).HasMaxLength(100);

                entity.Property(e => e.County).HasMaxLength(220);

                entity.Property(e => e.EntityType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Grantee).HasMaxLength(255);

                entity.Property(e => e.GrossAcres).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.Section).HasMaxLength(220);

                entity.Property(e => e.TractId).HasMaxLength(220);

                entity.Property(e => e.TractLength).HasColumnType("decimal(28, 6)");
            });

            modelBuilder.Entity<Obligation>(entity =>
            {
                entity.Property(e => e.FromDate).HasColumnType("date");

                entity.Property(e => e.Notes).IsUnicode(false);

                entity.Property(e => e.ObligationType).HasMaxLength(100);

                entity.Property(e => e.RecurringUnits).HasMaxLength(50);

                entity.Property(e => e.ToDate).HasColumnType("date");

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.Obligation)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK_Obligation_Contract");
            });

            modelBuilder.Entity<Ownership>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .HasMaxLength(50);

                entity.Property(e => e.OwnerName).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.TractId)
                    .HasColumnName("TractID")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Rate>(entity =>
            {
                entity.Property(e => e.ActualRent).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.FlatOrActualRateCalc)
                    .HasColumnType("decimal(28, 6)")
                    .HasComputedColumnSql("(case when ([FlatRate]=(0.0) OR [FlatRate] IS NULL) AND [ActualRent]<>(0.0) then [ActualRent] else [FlatRate] end)");

                entity.Property(e => e.FlatRate).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.RateDisplay)
                    .HasColumnType("decimal(28, 6)")
                    .HasComputedColumnSql("(case when [FlatRate]=(0.0) OR [FlatRate] IS NULL then case when [ActualRent]<>(0.0) then [ActualRent] else [RatePerUnit] end else [FlatRate] end)");

                entity.Property(e => e.RatePerUnit).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.RateUnitType).HasMaxLength(50);

                entity.Property(e => e.StringDisplay)
                    .IsRequired()
                    .HasMaxLength(13)
                    .HasComputedColumnSql("(case when [FlatRate]=(0.0) OR [FlatRate] IS NULL then case when [ActualRent]<>(0.0) then N'Actual Rent: ' else N'Per' end else N'Flat Rate: ' end)");

                entity.Property(e => e.UnitType).HasMaxLength(50);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.Rate)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK_Rate_Contract");
            });

            modelBuilder.Entity<Resolution>(entity =>
            {
                entity.Property(e => e.DueDate).HasColumnType("date");

                entity.Property(e => e.ResolutionDate).HasColumnType("date");

                entity.Property(e => e.ResolutionType).HasMaxLength(100);

                entity.HasOne(d => d.Obligation)
                    .WithMany(p => p.Resolution)
                    .HasForeignKey(d => d.ObligationId)
                    .HasConstraintName("FK_Resolution_Obligation");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.Resolution)
                    .HasForeignKey(d => d.TermId)
                    .HasConstraintName("FK_Resolution_Term");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.Property(e => e.Status1)
                    .HasColumnName("Status")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SurveyContract>(entity =>
            {
                entity.ToTable("Survey_Contract");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ContractType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.EntityType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Grantee)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Tpwr).HasColumnName("TPWR");
            });

            modelBuilder.Entity<Term>(entity =>
            {
                entity.Property(e => e.Expires).HasColumnType("date");

                entity.Property(e => e.TermType).HasMaxLength(50);

                entity.Property(e => e.TimePeriodUnits).HasMaxLength(50);

                entity.HasOne(d => d.Contract)
                    .WithMany(p => p.Term)
                    .HasForeignKey(d => d.ContractId)
                    .HasConstraintName("FK_Term_Contract");
            });

            modelBuilder.Entity<Tract>(entity =>
            {
                entity.HasIndex(e => e.TractId)
                    .HasName("TractDuplicate")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.GlobalId).HasMaxLength(1000);

                entity.Property(e => e.GrossAcres).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.Kv)
                    .HasColumnName("KV")
                    .HasColumnType("decimal(28, 6)");

                entity.Property(e => e.Npri).HasColumnName("NPRI");

                entity.Property(e => e.TractId)
                    .HasColumnName("TractID")
                    .HasMaxLength(220);

                entity.Property(e => e.TractLength).HasColumnType("decimal(28, 6)");

                entity.Property(e => e.TractType).HasMaxLength(220);

                entity.Property(e => e.TractWidth).HasColumnType("decimal(28, 6)");
            });

            modelBuilder.Entity<TypeAmountCategory>(entity =>
            {
                entity.ToTable("Type_AmountCategory");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.Prompt)
                    .HasColumnName("prompt")
                    .HasMaxLength(153)
                    .HasComputedColumnSql("(([Subcategory]+' - ')+[Description])");

                entity.Property(e => e.Subcategory).HasMaxLength(50);
            });

            modelBuilder.Entity<TypeContactDetails>(entity =>
            {
                entity.ToTable("Type_Contact_Details");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(220);
            });

            modelBuilder.Entity<TypeContract>(entity =>
            {
                entity.ToTable("Type_Contract");

                entity.Property(e => e.Category).HasMaxLength(150);

                entity.Property(e => e.ContractType).HasMaxLength(100);

                entity.Property(e => e.Tpwr).HasColumnName("TPWR");
            });

            modelBuilder.Entity<TypeExchange>(entity =>
            {
                entity.ToTable("Type_Exchange");

                entity.Property(e => e.ExchangeType).HasMaxLength(100);
            });

            modelBuilder.Entity<TypeObligation>(entity =>
            {
                entity.ToTable("Type_Obligation");

                entity.Property(e => e.ObligationType).HasMaxLength(100);
            });

            modelBuilder.Entity<TypePeriod>(entity =>
            {
                entity.ToTable("Type_Period");

                entity.Property(e => e.PeriodName).HasMaxLength(100);
            });
        }
    }
}
