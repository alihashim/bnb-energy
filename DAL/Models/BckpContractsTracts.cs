﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class BckpContractsTracts
    {
        public string BackupId { get; set; }
        public DateTime BackupDate { get; set; }
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public int? TractId { get; set; }
    }
}
