﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class BckpLandSurveys
    {
        public string BackupId { get; set; }
        public DateTime BackupDate { get; set; }
        public int Id { get; set; }
        public string County { get; set; }
        public string Survey { get; set; }
        public string Abstract { get; set; }
        public string Section { get; set; }
        public string Block { get; set; }
        public string Lot { get; set; }
        public string Notes { get; set; }
    }
}
