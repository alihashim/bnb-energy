﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class ContactDetails
    {
        public int Id { get; set; }
        public int? ContractEntityId { get; set; }
        public string ContactDetailsType { get; set; }
        public string Details { get; set; }

        public ContractEntity ContractEntity { get; set; }
    }
}
