﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Contract
    {
        public Contract()
        {
            HistoryStatus = new HashSet<HistoryStatus>();
            Obligation = new HashSet<Obligation>();
            Rate = new HashSet<Rate>();
            Term = new HashSet<Term>();
        }

        public int Id { get; set; }
        public int? ContractId { get; set; }
        public string PreviousId { get; set; }
        public string Grantee { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string ContractType { get; set; }
        public string Status { get; set; }
        public string FileLocation { get; set; }
        public string SiteAlias { get; set; }
        public bool? CurativeNeeded { get; set; }
        public bool? OptionToExtend { get; set; }
        public bool? ConsentToAssign { get; set; }
        public bool? HardConsent { get; set; }
        public bool? ConsentGiven { get; set; }
        public bool? Sublease { get; set; }
        public bool? Amended { get; set; }
        public bool? Assigned { get; set; }
        public bool? Tpwr { get; set; }
        public string Contact { get; set; }
        public string Notes { get; set; }
        public decimal? BasisAmount { get; set; }
        public string BasisUnits { get; set; }
        public bool? OptionExercised { get; set; }
        public double? GrossAcres { get; set; }
        public string ContactSafe { get; set; }
        public string GranteeSafe { get; set; }
        public bool? Evergreen { get; set; }
        public bool? Perpetual { get; set; }
        public string Grantor { get; set; }
        public string EntityType { get; set; }
        public int? TpwrId { get; set; }
        public string ContractIdText { get; set; }
        public int? Grantorid { get; set; }
        public int? Granteeid { get; set; }
        public int? Contactid { get; set; }
        public bool? Caliche { get; set; }
        public string Afe { get; set; }

        public ICollection<HistoryStatus> HistoryStatus { get; set; }
        public ICollection<Obligation> Obligation { get; set; }
        public ICollection<Rate> Rate { get; set; }
        public ICollection<Term> Term { get; set; }
    }
}
