﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class ContractRef
    {
        public int Id { get; set; }
        public int? SourceContract { get; set; }
        public int? ReferenceContract { get; set; }
        public string ReferenceContractId { get; set; }
        public string ReferenceContractType { get; set; }
        public string ReferenceGrantee { get; set; }
    }
}
