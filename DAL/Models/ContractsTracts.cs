﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class ContractsTracts
    {
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public int? TractId { get; set; }
    }
}
