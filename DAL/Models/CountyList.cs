﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class CountyList
    {
        public int Id { get; set; }
        public string CountyName { get; set; }
    }
}
