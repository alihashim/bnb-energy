﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class HistoryStatus
    {
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public string NewStatus { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? ModifiedOnDate { get; set; }
        public string UserEmail { get; set; }

        public Contract Contract { get; set; }
    }
}
