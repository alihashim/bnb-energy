﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class LandSurveys
    {
        public int Id { get; set; }
        public string County { get; set; }
        public string Survey { get; set; }
        public string Abstract { get; set; }
        public string Section { get; set; }
        public string Block { get; set; }
        public string Lot { get; set; }
        public string Notes { get; set; }
        public string SectionBlockLot { get; set; }
        public string CountyAbstract { get; set; }
        public string SectionText { get; set; }
        public int? AssetId { get; set; }
        public string AutoCompleteDisplay { get; set; }
    }
}
