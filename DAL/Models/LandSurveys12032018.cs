﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class LandSurveys12032018
    {
        public int Objectid { get; set; }
        public string County { get; set; }
        public string Block { get; set; }
        public string Section { get; set; }
        public string Abstract { get; set; }
        public string Survey { get; set; }
        public int? AssetId { get; set; }
    }
}
