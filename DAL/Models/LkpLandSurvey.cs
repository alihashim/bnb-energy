﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class LkpLandSurvey
    {
        public int Id { get; set; }
        public string AutoCompleteDisplay { get; set; }
        public string County { get; set; }
        public string Abstract { get; set; }
        public string Section { get; set; }
        public string Block { get; set; }
        public bool? HasTpwr { get; set; }
        public int? LandSurveyId { get; set; }
        public string AssetName { get; set; }
        public string Survey { get; set; }
        public string Lot { get; set; }
    }
}
