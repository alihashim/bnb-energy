﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class LkpSurveyTract
    {
        public int Id { get; set; }
        public int? LandSurveyId { get; set; }
        public int? TractKey { get; set; }
        public string TractId { get; set; }
        public string TractTypeDescription { get; set; }
        public decimal? GrossAcres { get; set; }
        public double? Npri { get; set; }
        public bool? HasTpwr { get; set; }
    }
}
