﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class LkpTract
    {
        public int Id { get; set; }
        public int? TractKey { get; set; }
        public string TractId { get; set; }
        public string TractType { get; set; }
        public bool? HasTpwr { get; set; }
    }
}
