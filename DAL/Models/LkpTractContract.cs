﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class LkpTractContract
    {
        public int Id { get; set; }
        public int? ContractKey { get; set; }
        public int? TractKey { get; set; }
        public int? ContractId { get; set; }
        public string Grantee { get; set; }
        public string ContractType { get; set; }
        public decimal? GrossAcres { get; set; }
        public string EntityType { get; set; }
        public bool? HasTpwr { get; set; }
        public string TractId { get; set; }
        public decimal? TractLength { get; set; }
        public string County { get; set; }
        public string Block { get; set; }
        public string Section { get; set; }
        public int? AssetId { get; set; }
        public string ContractIdText { get; set; }
    }
}
