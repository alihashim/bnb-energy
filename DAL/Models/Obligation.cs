﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Obligation
    {
        public Obligation()
        {
            Resolution = new HashSet<Resolution>();
        }

        public int Id { get; set; }
        public int? ContractId { get; set; }
        public string ObligationType { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? RecurringPeriod { get; set; }
        public string RecurringUnits { get; set; }
        public string Notes { get; set; }

        public Contract Contract { get; set; }
        public ICollection<Resolution> Resolution { get; set; }
    }
}
