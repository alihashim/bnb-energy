﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Ownership
    {
        public int Id { get; set; }
        public string OwnerName { get; set; }
        public double? OwnerInterest { get; set; }
        public string ContractId { get; set; }
        public string TractId { get; set; }
        public string Status { get; set; }
    }
}
