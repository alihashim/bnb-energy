﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Rate
    {
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public decimal? FlatRate { get; set; }
        public decimal? RatePerUnit { get; set; }
        public string UnitType { get; set; }
        public string RateUnitType { get; set; }
        public string Notes { get; set; }
        public decimal? ActualRent { get; set; }
        public decimal? FlatOrActualRateCalc { get; set; }
        public decimal? RateDisplay { get; set; }
        public string StringDisplay { get; set; }

        public Contract Contract { get; set; }
    }
}
