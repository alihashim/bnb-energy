﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Resolution
    {
        public int Id { get; set; }
        public int? ObligationId { get; set; }
        public int? TermId { get; set; }
        public DateTime? ResolutionDate { get; set; }
        public string ResolutionType { get; set; }
        public string ResolutionDescription { get; set; }
        public DateTime? DueDate { get; set; }

        public Obligation Obligation { get; set; }
        public Term Term { get; set; }
    }
}
