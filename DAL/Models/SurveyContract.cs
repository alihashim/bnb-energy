﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class SurveyContract
    {
        public int Id { get; set; }
        public int LandSurveyId { get; set; }
        public int ContractKey { get; set; }
        public int ContractId { get; set; }
        public string Grantee { get; set; }
        public string ContractType { get; set; }
        public double TotalContractAcres { get; set; }
        public string EntityType { get; set; }
        public bool Tpwr { get; set; }
        public bool HasTpwr { get; set; }
    }
}
