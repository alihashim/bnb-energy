﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Term
    {
        public Term()
        {
            Resolution = new HashSet<Resolution>();
        }

        public int Id { get; set; }
        public int? ContractId { get; set; }
        public string TermType { get; set; }
        public int? TimePeriod { get; set; }
        public string TimePeriodUnits { get; set; }
        public DateTime? Expires { get; set; }
        public string Notes { get; set; }
        public int? ObligationId { get; set; }
        public int? Sequence { get; set; }

        public Contract Contract { get; set; }
        public ICollection<Resolution> Resolution { get; set; }
    }
}
