﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class Tract
    {
        public int Id { get; set; }
        public string TractId { get; set; }
        public decimal? GrossAcres { get; set; }
        public bool? OwnSurface { get; set; }
        public bool? GrazingLease { get; set; }
        public bool? SurfaceLease { get; set; }
        public bool? Easements { get; set; }
        public double? Npri { get; set; }
        public int? LandSurveyId { get; set; }
        public string TractType { get; set; }
        public decimal? TractLength { get; set; }
        public decimal? TractWidth { get; set; }
        public bool? MineralClassified { get; set; }
        public decimal? Kv { get; set; }
        public string LandSurveyDescription { get; set; }
        public string Notes { get; set; }
        public string TractTypeDescription { get; set; }
        public string GlobalId { get; set; }
    }
}
