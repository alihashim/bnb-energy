﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class TypeAmountCategory
    {
        public int Id { get; set; }
        public int? Category { get; set; }
        public string Subcategory { get; set; }
        public string Description { get; set; }
        public string Prompt { get; set; }
    }
}
