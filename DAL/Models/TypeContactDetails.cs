﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class TypeContactDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
