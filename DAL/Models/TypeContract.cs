﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class TypeContract
    {
        public int Id { get; set; }
        public string ContractType { get; set; }
        public string Category { get; set; }
        public bool? Tpwr { get; set; }
    }
}
