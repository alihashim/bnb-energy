﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class TypeExchange
    {
        public int Id { get; set; }
        public string ExchangeType { get; set; }
    }
}
