﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class TypeObligation
    {
        public int Id { get; set; }
        public string ObligationType { get; set; }
    }
}
