﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class TypePeriod
    {
        public int Id { get; set; }
        public string PeriodName { get; set; }
    }
}
